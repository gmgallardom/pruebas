package com.inventary.client.controller;

import com.inventary.client.model.Client;
import com.inventary.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/all")
    public Flux<Client> findAll() {
        return clientService.findAll();
    }

    @PostMapping("/create")
    public Mono<Client> createClient(@RequestBody Client client) {
        return clientService.createClient(client);
    }

}
