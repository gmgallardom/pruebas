package com.inventary.client.service.impl;

import com.inventary.client.model.Client;
import com.inventary.client.repository.ClientRepository;
import com.inventary.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Flux<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public Mono<Client> createClient(Client client) {
        return clientRepository.save(client);
    }
}
