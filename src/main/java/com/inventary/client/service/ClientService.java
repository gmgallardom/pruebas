package com.inventary.client.service;

import com.inventary.client.model.Client;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClientService {

    public Flux<Client> findAll();
    public Mono<Client> createClient(Client client);

}
